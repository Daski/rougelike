﻿using Assets.Scripts.Managers;
using UnityEngine;

namespace Assets.Scripts
{
	public class Loader : MonoBehaviour
	{
		public GameObject gameManager;

		private void Awake()
		{
			if(GameManager.Instance == null)
			{
				Instantiate(gameManager);
			}
		}
	}
}
