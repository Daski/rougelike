﻿using Assets.Scripts.Mechanics;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.Managers
{
	public class GameManager : MonoBehaviour
	{
		private static GameManager instance;

		private int level = 1;
		private List<Enemy> enemies;
		private bool enemiesMoving;
		private Text levelText;
		private GameObject levelImage;
		private bool doingSetup;

		public float LevelStartDelay = 2f;
		public float TurnDelay = 0.1f;
		public BoardManager BoardManager;
		public int PlayerFoodPoints = 100;

		[HideInInspector]
		public bool playerTurn = true;

		public static GameManager Instance
		{
			get
			{
				return instance;
			}
		}

		private void Awake()
		{
			if (instance != null && instance != this)
			{
				Destroy(this.gameObject);
			}
			else
			{
				instance = this;
			}

			DontDestroyOnLoad(this.gameObject);

			enemies = new List<Enemy>();
			BoardManager = GetComponent<BoardManager>();
			InitGame();
		}

		private void Update()
		{
			if(playerTurn || enemiesMoving || doingSetup)
			{
				return;
			}

			StartCoroutine(MoveEnemies());
		}

		private void OnLevelWasLoaded(int index)
		{
			level++;
			InitGame();
		}

		public void GameOver()
		{
			levelText.text = $"After {level} days, you straved.";
			levelImage.SetActive(true);
			this.enabled = false;
		}

		public void AddEnemyToLost(Enemy enemy)
		{
			enemies.Add(enemy);
		}

		private void InitGame()
		{
			doingSetup = true;
			levelImage = GameObject.Find("LevelImage");
			levelText = GameObject.Find("LevelText").GetComponent<Text>();
			levelText.text = $"Day: {level}";
			levelImage.SetActive(true);
			Invoke("HideLevelImage", LevelStartDelay);

			enemies.Clear();
			BoardManager.SetupScene(level);
		}

		private void HideLevelImage()
		{
			levelImage.SetActive(false);
			doingSetup = false;
		}

		private IEnumerator MoveEnemies()
		{
			enemiesMoving = true;
			yield return new WaitForSeconds(TurnDelay);

			if(enemies.Count == 0)
			{
				yield return new WaitForSeconds(TurnDelay);
			}

			for (int i = 0; i < enemies.Count; i++)
			{
				enemies[i].MoveEnemy();
				yield return new WaitForSeconds(enemies[i].moveTime);
			}

			playerTurn = true;
			enemiesMoving = false;
		}
	}
}

