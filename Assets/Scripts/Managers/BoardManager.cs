﻿using Assets.Scripts.Helpers;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Assets.Scripts.Managers
{
	public class BoardManager : MonoBehaviour
	{
		private Transform boardHolder;
		private IList<Vector3> gridPositions = new List<Vector3>();

		public int Columns = 8;
		public int Rows = 8;
		public Count wallCount = new Count(5, 9);
		public Count foodCount = new Count(1, 5);
		public GameObject Exit;
		public GameObject[] FloorTiles;
		public GameObject[] WallTiles;
		public GameObject[] FoodTiles;
		public GameObject[] EnemyTiles;
		public GameObject[] OuterWallTiles;

		public void SetupScene(int level)
		{
			BoardSetup();
			InitList();
			LayoutObjectAtRandom(WallTiles, wallCount.Min, wallCount.Max);
			LayoutObjectAtRandom(FoodTiles, foodCount.Min, foodCount.Max);

			int enemyCount = (int)Mathf.Log(level, 2f);
			LayoutObjectAtRandom(EnemyTiles, enemyCount, enemyCount);
			Instantiate(Exit, new Vector3(Columns - 1, Rows - 1, 0f), Quaternion.identity);
		}

		private void InitList()
		{
			gridPositions.Clear();

			for (int x = 0; x < Columns - 1; x++)
			{
				for (int y = 0; y < Rows - 1; y++)
				{
					gridPositions.Add(new Vector3(x, y, 0f));
				}
			}
		}

		private void BoardSetup()
		{
			boardHolder = new GameObject("Board").transform;

			for (int x = -1; x < Columns + 1; x++)
			{
				for (int y = -1; y < Rows + 1; y++)
				{
					var toInstantiate = FloorTiles[Random.Range(0, FloorTiles.Length)];
					if (x == -1 || x == Columns || y == -1 || y == Rows)
					{
						toInstantiate = OuterWallTiles[Random.Range(0, OuterWallTiles.Length)];
					}

					var insatnce = Instantiate(toInstantiate, new Vector3(x, y, 0f), Quaternion.identity);
					insatnce.transform.SetParent(boardHolder);
				}
			}
		}

		private Vector3 RandomPoisiton()
		{
			int randomIndex = Random.Range(0, gridPositions.Count);
			var randomPosition = gridPositions[randomIndex];
			gridPositions.RemoveAt(randomIndex);

			return randomPosition;
		}

		private void LayoutObjectAtRandom(GameObject[] tileArray, int min, int max)
		{
			int objectCount = Random.Range(min, max + 1);

			for (int i = 0; i < objectCount; i++)
			{
				var randomPosition = RandomPoisiton();
				var tileChoice = tileArray[Random.Range(0, tileArray.Length)];
				Instantiate(tileChoice, randomPosition, Quaternion.identity);
			}
		}
	}
}
