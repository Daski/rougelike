﻿using UnityEngine;

public class SoundManager : MonoBehaviour
{
	private static SoundManager instance = null;

	public AudioSource FxSource;
	public AudioSource MusicSource;
	public float lowPitchRange = 0.95f;
	public float highPitchRange = 1.05f;

	public static SoundManager Instance
	{
		get
		{
			return instance;
		}
	}

	public void PlaySingle(AudioClip clip)
	{
		FxSource.clip = clip;
		FxSource.Play();
	}

	public void RandomFx(params AudioClip[] clips)
	{
		int randomIndex = Random.Range(0, clips.Length);
		float randomPitch = Random.Range(lowPitchRange, highPitchRange);

		FxSource.pitch = randomPitch;
		FxSource.clip = clips[randomIndex];
		FxSource.Play();
	}

	#region Engine
	private void Awake()
	{
		if (instance != null && instance != this)
		{
			Destroy(this.gameObject);
		}
		else
		{
			instance = this;
		}

		DontDestroyOnLoad(instance);
	}
	#endregion Engine


}
