﻿using System;

namespace Assets.Scripts.Helpers
{
	[Serializable]
	public class Count
	{
		public int Min;
		public int Max;

		public Count(int min, int max)
		{
			Min = min;
			Max = max;
		}
	}
}
