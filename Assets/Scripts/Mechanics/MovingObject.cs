﻿using System.Collections;
using UnityEngine;

namespace Assets.Scripts.Mechanics
{
	public abstract class MovingObject : MonoBehaviour
	{
		public float moveTime = 0.1f;
		public LayerMask blockingLayer;

		private BoxCollider2D collider;
		private Rigidbody2D rigidbody;
		private float inverseMoveTime;

		protected virtual void Start()
		{
			collider = this.GetComponent<BoxCollider2D>();
			rigidbody = this.GetComponent<Rigidbody2D>();
			inverseMoveTime = 1f / moveTime;
		}

		protected bool Move(int xDir, int yDir, out RaycastHit2D hit)
		{
			Vector2 start = this.transform.position;
			Vector2 end = start + new Vector2(xDir, yDir);

			collider.enabled = false;
			hit = Physics2D.Linecast(start, end, blockingLayer);
			collider.enabled = true;

			if(hit.transform == null)
			{
				StartCoroutine(SmoothMovement(end));
				return true;
			}

			return false;
		}

		protected virtual void AttepmtToMove<T>(int xDir, int yDir)
			where T : Component
		{
			RaycastHit2D hit;
			bool canMove = Move(xDir, yDir, out hit);

			if(hit.transform == null)
			{
				return;
			}

			T hitComponent = hit.transform.GetComponent<T>();
			if(!canMove && hitComponent != null)
			{
				OnCantMove(hitComponent);
			}
		}
		
		protected IEnumerator SmoothMovement(Vector3 end)
		{
			float sqrRemainingDistance = (this.transform.position - end).sqrMagnitude;
			while (sqrRemainingDistance > float.Epsilon)
			{
				Vector3 newPosition = Vector3.MoveTowards(rigidbody.position, end, inverseMoveTime * Time.deltaTime);
				rigidbody.MovePosition(newPosition);
				sqrRemainingDistance = (transform.position - end).sqrMagnitude;

				yield return null;
			}
		}

		protected abstract void OnCantMove<T>(T component) 
			where T : Component;
	}
}
