﻿using Assets.Scripts.Managers;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.Mechanics
{
	public class Player : MovingObject
	{
		private GameManager gameManager;
		private SoundManager soundManager;
		private Animator animator;
		private int food;

		public int WallDamage = 1;
		public int PointsPerFood = 10;
		public int PointsPerSoda = 20;
		public float RestartLevelDelay = 1f;
		public Text FoodText;
		public AudioClip MoveSound1;
		public AudioClip MoveSound2;
		public AudioClip EatSound1;
		public AudioClip EatSound2;
		public AudioClip DrinkSound1;
		public AudioClip DrinkSound2;
		public AudioClip GameOverSound;

		public void LoseFood(int foodToLose)
		{
			animator.SetTrigger("playerHit");
			food -= foodToLose;
			RefreshFoodText();
			CheckIfGameOver();
		}

		protected override void OnCantMove<T>(T component)
		{
			Wall hitWall = component as Wall;
			hitWall.DamageWall(WallDamage);

			animator.SetTrigger("playerChop");
		}

		protected override void AttepmtToMove<T>(int xDir, int yDir)
		{
			food--;
			RefreshFoodText();

			base.AttepmtToMove<T>(xDir, yDir);

			RaycastHit2D hit;
			if(Move(xDir, yDir, out hit))
			{
				soundManager.RandomFx(MoveSound1, MoveSound2);
			}

			CheckIfGameOver();

			gameManager.playerTurn = false;
		}

		private void CheckIfGameOver()
		{
			if (food <= 0)
			{
				soundManager.PlaySingle(GameOverSound);
				soundManager.MusicSource.Stop();
				gameManager.GameOver();
			}
		}

		private void Restart()
		{
			Application.LoadLevel(Application.loadedLevel);
		}

		private void RefreshFoodText()
		{
			FoodText.text = $"Food: {food}";
		}

		#region Engine
		protected override void Start()
		{
			animator = this.GetComponent<Animator>();

			soundManager = SoundManager.Instance;
			gameManager = GameManager.Instance;
			food = gameManager.PlayerFoodPoints;
			RefreshFoodText();
			base.Start();
		}

		private void Update()
		{
			if (!gameManager.playerTurn) return;

			int horizontal = 0;
			int vertical = 0;

			horizontal = (int)Input.GetAxisRaw("Horizontal");
			vertical = (int)Input.GetAxisRaw("Vertical");

			if (horizontal != 0)
			{
				vertical = 0;
			}

			if (horizontal != 0 || vertical != 0)
			{
				AttepmtToMove<Wall>(horizontal, vertical);
			}

		}

		private void OnTriggerEnter2D(Collider2D collision)
		{
			if (collision.tag.Equals("Exit"))
			{
				Invoke("Restart", RestartLevelDelay);
				this.enabled = false;
			}
			else if (collision.tag.Equals("Food"))
			{
				food += PointsPerFood;
				RefreshFoodText();
				soundManager.RandomFx(EatSound1, EatSound2);
				collision.gameObject.SetActive(false);
			}
			else if (collision.tag.Equals("Soda"))
			{
				food += PointsPerSoda;
				RefreshFoodText();
				soundManager.RandomFx(DrinkSound1, DrinkSound2);
				collision.gameObject.SetActive(false);
			}
		}

		private void OnDisable()
		{
			gameManager.PlayerFoodPoints = food;
		}
		#endregion Engine
	}
}
