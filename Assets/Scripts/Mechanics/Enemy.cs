﻿using Assets.Scripts.Managers;
using UnityEngine;

namespace Assets.Scripts.Mechanics
{
	public class Enemy : MovingObject
	{
		private GameManager gameManager;
		private SoundManager soundManager;
		private Animator animator;
		private Transform target;
		private bool skipMove;

		public int PlayerDamage;
		public AudioClip enemyAttack1;
		public AudioClip enemyAttack2;

		protected override void Start()
		{
			soundManager = SoundManager.Instance;
			gameManager = GameManager.Instance;
			gameManager.AddEnemyToLost(this);
			animator = this.GetComponent<Animator>();
			target = GameObject.FindGameObjectWithTag("Player").transform;
			base.Start();
		}

		protected override void OnCantMove<T>(T component)
		{
			Player hitPlayer = component as Player;
			animator.SetTrigger("enemyAttack");
			hitPlayer.LoseFood(PlayerDamage);
			soundManager.RandomFx(enemyAttack1, enemyAttack2);
		}

		protected override void AttepmtToMove<T>(int xDir, int yDir)
		{
			if(skipMove)
			{
				skipMove = false;
				return;
			}

			base.AttepmtToMove<T>(xDir, yDir);

			skipMove = true;
		}

		public void MoveEnemy()
		{
			int xDir = 0;
			int yDir = 0;

			if(Mathf.Abs(target.position.x - this.transform.position.x) < float.Epsilon)
			{
				yDir = target.position.y > this.transform.position.y ? 1 : -1;
			}
			else
			{
				xDir = target.position.x > this.transform.position.x ? 1 : -1;
			}

			AttepmtToMove<Player>(xDir, yDir);
		}
	}
}
