﻿using UnityEngine;

namespace Assets.Scripts.Mechanics
{
	public class Wall : MonoBehaviour
	{
		private SoundManager soundManager;

		public Sprite DmgSprite;
		public int HP = 4;
		public AudioClip ChopSound1;
		public AudioClip ChopSound2;

		private SpriteRenderer renderer;

		private void Awake()
		{
			renderer = this.GetComponent<SpriteRenderer>();
			soundManager = SoundManager.Instance;
		}

		public void DamageWall(int hpToLoss)
		{
			soundManager.RandomFx(ChopSound1, ChopSound2);
			renderer.sprite = DmgSprite;
			HP -= hpToLoss;
			if(HP <= 0)
			{
				this.gameObject.SetActive(false);
			}
		}
	}
}
